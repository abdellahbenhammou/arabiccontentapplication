package droid.father.arabiccontentapp;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;



public class MainActivity extends Activity implements OnNavigationListener {
	final Context context = this;
	ImageView iv = null;
	TextView cat = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		cat = (TextView) findViewById(R.id.textView1);
		
		SlidingMenu menu;
		menu = new SlidingMenu(this);
		menu.setMode(SlidingMenu.LEFT);
		menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		menu.setShadowWidth(5);
		menu.setFadeEnabled(true);
		menu.setFadeDegree(0.4f);
		menu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		Display screen = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		screen.getSize(size);
		int behindWidth = size.x / 2;
		menu.setBehindWidth(behindWidth);
		menu.setMenu(R.layout.menu_frame);
		menu.setShadowWidth(30);
		final ListView listview = (ListView) findViewById(R.id.menu);
		String[] values = new String[] { "تاريخ", "جغرافيا", "أكل"};

		final ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < values.length; ++i) {
			list.add(values[i]);
		}
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);
		
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				TextView tv = (TextView) arg1;
				//Toast.makeText(getApplicationContext(), "Menu clicked: "+tv.getText().toString()+" - "+arg2+" - "+arg3, Toast.LENGTH_SHORT).show();
				if(tv.getText().toString().equals("تاريخ")){
					//-----------------------------
					cat.setText("تاريخ");

				}
				else if(tv.getText().toString().equals("جغرافيا")){
					//-----------------------------
					cat.setText("جغرافيا");

				}
				else if(tv.getText().toString().equals("أكل")){
					//-----------------------------
					cat.setText("أكل");

				}
			}
		});

		ActionBar ab = getActionBar();
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		ab.setDisplayShowTitleEnabled(false);
		final String[] countries = {"المغرب", "تونس", "السعودية" };
		ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ab.getThemedContext(), android.R.layout.simple_spinner_item, countries);
		arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		ab.setListNavigationCallbacks(arrayAdapter, this);
		
		iv = (ImageView) findViewById(R.id.imageView1);
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}



	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {
		// TODO Auto-generated method stub
		//Toast.makeText(getApplicationContext(), "clicked: " + arg0, Toast.LENGTH_SHORT).show();
		if(arg0 == 0)
			iv.setImageResource(R.drawable.mroccoflag);
		else if(arg0 == 1)			
			iv.setImageResource(R.drawable.tunisia_flag);
		else if(arg0 == 2)
			iv.setImageResource(R.drawable.saudi_arabia_flag);
		return true;
	}

}
